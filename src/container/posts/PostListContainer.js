import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toast } from 'react-toastify';

import ListPost from '../../components/posts/ListPost';
import { getPosts } from '../../redux/actions/postActions';
import { destroyPost } from '../../requests/postRequests';

class PostListContainer extends React.Component {

  componentDidMount() {
    this.props.getPosts()
  }

  deletePost = async (postId) => {
    try {
      await destroyPost(postId);
      toast.success('Post apagado!!');
      this.props.getPosts();
    } catch (error) {
      toast.error('Não foi possível apagar o post!');
      console.log('error', error);
    }
  }

  render() {
    return (
      <ListPost
        posts={this.props.posts}
        deletePost={this.deletePost}
      />
    );
  }
}


const mapStateToProps = state => (
  {
    posts: state.postReducer
  }
)

const mapDispatchToProps = dispatch => (
  bindActionCreators({ getPosts }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(PostListContainer);
