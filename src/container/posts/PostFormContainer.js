import React from 'react';
import { toast } from 'react-toastify';

import PostForm from '../../components/posts/PostForm';
import { createPost } from '../../requests/postRequests';
import { navigate } from '@reach/router';

class PostFormContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const params = {
      title: this.state.title,
      description: this.state.description
    };
    try {
      await createPost(params);
      toast.success('Post criado com sucesso');
      this.setState({ title: '', description: '' });
      setTimeout(() => {
        navigate('/');
      }, 500);
    } catch (error) {
      toast.error('Não foi possível criar o post');
      console.log('error', error);
    }

  }

  handleInput = (event, input) => {
    switch(input) {
      case 'title': {
        this.setState({ title: event.target.value });
        break;
      }
      case 'description': {
        this.setState({ description: event.target.value });
        break;
      }
      default:
        break;
    }
  }

  render() {
    return (
      <PostForm
        title={this.state.title}
        description={this.state.description}
        handleInput={this.handleInput}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default PostFormContainer;