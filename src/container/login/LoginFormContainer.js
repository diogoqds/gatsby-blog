import React from 'react';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import LoginForm from '../../components/login/LoginForm';
import { loginRequest } from '../../requests/loginRequest';
import { login } from '../../redux/actions/authenticationActions';
import { navigate } from '@reach/router';

class PostFormContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const params = {
      email: this.state.email,
      password: this.state.password
    };
    try {
      const response = await loginRequest(params);
      const userInfo = {
        ...response.data.user,
        token: response.data.access_token
      }
      this.props.login(userInfo);
      toast.success('Login realizado com sucesso!!');
      this.setState({ email: '', password: '' });
      setTimeout(() => {
        navigate('/');
      }, 500);
    } catch (error) {
      toast.error('Não foi possível fazer o login!');
      console.log('error', error);
    }

  }

  handleInput = (event, input) => {
    switch(input) {
      case 'email': {
        this.setState({ email: event.target.value });
        break;
      }
      case 'password': {
        this.setState({ password: event.target.value });
        break;
      }
      default:
        break;
    }
  }

  render() {
    return (
      <LoginForm
        email={this.state.email}
        password={this.state.password}
        handleInput={this.handleInput}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({ login }, dispatch)
);

export default connect(null, mapDispatchToProps)(PostFormContainer);