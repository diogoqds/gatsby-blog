import axios from 'axios';
import { toast } from 'react-toastify';

export function responseInterceptor(store) {
  return axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    if(error.response.status === 401) {
      toast.error('Sua sessão expirou. Faça login novamente!');
      store.dispatch({ type: 'LOGOUT' });
    }
    // Do something with response error/
    return Promise.reject(error);
  });
}