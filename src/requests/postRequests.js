import { requestAuth, requestNotAuth } from './request';

export function createPost(params) {
  const config = {
    method: 'POST',
    url: 'posts',
    data: { post: {...params }},
  }
  return requestAuth(config)
}

export function allPosts() {
  const config = {
    method: 'GET',
    url: 'posts'
  }
  return requestNotAuth(config);
}

export function destroyPost(postId) {
  const config = {
    method: 'DELETE',
    url: `posts/${postId}`,
  }

  return requestAuth(config);
}

export function getPost(postId) {
  const config = {
    method: 'GET',
    url: `posts/${postId}`
  }

  return requestAuth(config);
}