import { requestNotAuth } from './request';

export function loginRequest(params) {
  const config = {
    method: 'POST',
    data: params,
    url: 'auth/login'
  }
  return requestNotAuth(config);
}