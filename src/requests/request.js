import axios from 'axios';
import { store } from '../redux';

const API_ROUTE = 'http://localhost:3000/';


function request({ method = 'GET', url = '', data = {}, headers = {} }) {
  const config = {
    method,
    url: API_ROUTE + url,
    data,
    headers: {
      contentType: 'application/json',
      ...headers
    }
  }

  return axios(config);
}

export function requestNotAuth({ method = 'GET', url = '', data = {}, headers = {} }) {
  const config = {
    method,
    url,
    data,
    headers: {
      contentType: 'application/json',
      ...headers
    }
  }

  return request(config);
}

export function requestAuth({ method = 'GET', url = '', data = {}, headers = {} }) {
  const config = {
    method,
    url,
    data,
    headers: {
      contentType: 'application/json',
      Authorization: `Bearer ${store.getState().authenticationReducer.user.token}`,
      ...headers
    }
  }

  return request(config);
}