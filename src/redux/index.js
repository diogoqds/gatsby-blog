import { combineReducers, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { postReducer } from './reducers/postReducer';
import { authenticationReducer } from './reducers/authenticationReducer';

const reducers = combineReducers({
  postReducer,
  authenticationReducer
})
const middlewares = [thunk];

let store = {};
if(process.env.GATSBY_ENVIROMENT === 'development') {
  store = createStore(reducers, composeWithDevTools(
    applyMiddleware(...middlewares),
  ));
} else {
  store = createStore(reducers, applyMiddleware(...middlewares));
}

export { store };
