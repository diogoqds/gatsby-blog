const INITIAL_STATE = [];

export function postReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case 'ADD_POSTS': {
      return [...action.payload]
    }
    default:
      return state;
  }
}