import { store } from '../index';

const INITIAL_STATE = {
  logged: false,
  user: { id: '', name: '', email: '', token: '' }
}

export function authenticationReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case 'LOGIN': {
      createSession(action.payload)
      return { logged: true, user: {...action.payload} }
    }
    case 'LOGOUT': {
      removeSession();
      return { logged: false, user: { id: '', name: '', email: '', token: '' } }
    }
    default:
      return state;
  }
}

export function createSession(userInfo) {
  sessionStorage.setItem('userInfo', JSON.stringify(userInfo))
}

export function getSession() {
  try {
    const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
    if(userInfo !== null) {
      store.dispatch({ type: 'LOGIN', payload: userInfo })
    }
  } catch (error) {
  }
}

export function removeSession() {
  sessionStorage.removeItem('userInfo');
}