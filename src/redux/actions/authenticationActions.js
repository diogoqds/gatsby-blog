export function login(payload) {
  return dispatch => {
    dispatch({ type: 'LOGIN', payload });
  }
}

export function logout() {
  return dispatch => {
    dispatch({ type: 'LOGOUT' });
  }
}