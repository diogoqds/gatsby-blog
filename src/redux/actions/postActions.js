import { allPosts } from '../../requests/postRequests';

export function getPosts() {
  return async (dispatch) => {
    const response = await allPosts();
    dispatch({ type: 'ADD_POSTS', payload: response.data });
  }
}