export const getId = (props, regex = /\d{1,999}/) => {
  const path = props.location.pathname;
  const id = path.match(regex)[0];
  return id;
}