import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/share/Layout'
import SEO from '../components/share/Seo'
import PostListContainer from '../container/posts/PostListContainer';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <PostListContainer />
    <Link to="/posts/new/">Criar Post</Link> <br/>
  </Layout>
)
export default IndexPage;
