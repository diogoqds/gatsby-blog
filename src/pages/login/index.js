import React from 'react';
import Layout from '../../components/share/Layout';
import LoginFormContainer from '../../container/login/LoginFormContainer';

const Login = () => (
  <Layout>
    <LoginFormContainer />
  </Layout>
);

export default Login;
