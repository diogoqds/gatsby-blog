import React from 'react';
import { toast } from 'react-toastify';

import Layout from '../../components/share/Layout';
import { getId } from '../../helpers/getIdUrl';
import { getPost } from '../../requests/postRequests';
import Post from '../../components/posts/Post';

class ShowPostPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: getId(props),
      title: '',
      description: ''
    };
  }

  async componentDidMount() {
    try {
      const response = await getPost(this.state.id);
      const { id, title, description } = response.data;
      this.setState({ id, title, description });
    } catch (error) {
      console.log('error', error);
      toast.error('Não foi possível pegar os dados!');
    }
  }

  render() {
    return (
      <Layout>
        <Post {...this.state} />
      </Layout>
    );
  }
}

export default ShowPostPage;
