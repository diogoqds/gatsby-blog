import React from 'react';
import Layout from '../../components/share/Layout';
import PostFormContainer from '../../container/posts/PostFormContainer';
import PrivatePage from '../../components/share/PrivatePage';

const NewPostPage = () => (
  <PrivatePage>
    <Layout>
      <PostFormContainer />
    </Layout>
  </PrivatePage>
)

export default NewPostPage;
