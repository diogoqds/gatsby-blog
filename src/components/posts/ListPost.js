import React from 'react';
import { Link } from 'gatsby';

import List from '../share/List';
import Card from '../share/Card';
import IfLogged from '../share/IfLogged';
import Post from './Post';

const ListPost = (props) => {
  return (
    <List>
      {
        props.posts.map((post) => (
          <Card key={post.id}>
            <Post
              title={post.title}
              description={post.description}
            />
            <IfLogged>
              <p onClick={() => props.deletePost(post.id)}>Apagar</p>
            </IfLogged>
            <Link to={`posts/show/${post.id}`}>
              Ver mais
            </Link>
          </Card>
        ))
      }
    </List>
  );
}

export default ListPost;