import React from 'react';
import PropTypes from 'prop-types';

const Post = (props) => (
  <div>
    <p><b>Title:</b> {props.title}</p>
    <p><b>Description:</b> {props.description}</p>
  </div>
);

Post.PropTypes = {
  title: PropTypes.string,
  description: PropTypes.string
};

export default Post;