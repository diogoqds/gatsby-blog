import React from 'react';

const PostForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div>
        <label htmlFor='title'>Title</label> <br/>
        <input autoFocus type='text' value={props.title} onChange={(event) => props.handleInput(event, 'title')}/>
      </div>

      <div>
        <label htmlFor='description'>Description</label> <br />
        <textarea value={props.description} onChange={(event) => props.handleInput(event, 'description')}/>
      </div>

      <button type='submit'>Send</button>
    </form>
  );
}

export default PostForm;