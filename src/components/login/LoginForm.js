import React from 'react';

const LoginForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>

      <div>
        <label htmlFor='email'>Email</label><br/>
        <input type='email' id='email' value={props.email} onChange={(event) => props.handleInput(event, 'email')} />
      </div>

      <div>
        <label htmlFor='password'>Password</label><br/>
        <input type='password' id='password' value={props.password} onChange={(event) => props.handleInput(event, 'password')} />
      </div>

      <button type='submit'>Login</button>
    </form>
  );
}

export default LoginForm;
