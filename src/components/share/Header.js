import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toast } from 'react-toastify';

import { logout } from '../../redux/actions/authenticationActions';
import IfLogged from './IfLogged';
import NotLogged from './NotLogged';

class Header extends React.Component {

  logout = () => {
    toast.success('Logout realizado com sucesso!');
    this.props.logout();
  }

  render() {
    const { siteTitle } = this.props;
    return (
      <div
        style={{
          background: `rebeccapurple`,
          marginBottom: `1.45rem`,
        }}
      >
        <div
          style={{
            margin: `0 auto`,
            maxWidth: 960,
            padding: `1.45rem 1.0875rem`,
          }}
        >
          <h1 style={{ margin: 0 }}>
            <Link
              to="/"
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              {siteTitle}
            </Link>
          </h1>
          <NotLogged>
            <Link to="/login/" style={{ color: 'white' }}>
              Login
            </Link>
          </NotLogged>
          <IfLogged>
            <div onClick={this.logout}><p style={{ color: 'white', cursor: 'pointer' }}>Logout</p></div>
          </IfLogged>
        </div>
      </div>
    )
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({ logout }, dispatch)
)

export default connect(null, mapDispatchToProps)(Header);
