import React from 'react';

const List = (props) => (
  <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', width: '100%', height: 'auto'}}>
    {props.children}
  </div>
);

export default List;
