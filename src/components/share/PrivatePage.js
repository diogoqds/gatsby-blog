import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from '@reach/router';

class PrivatePage extends React.Component {
  render() {
    if(this.props.authentication && !this.props.authentication.logged) {
      return <Redirect to='/' noThrow />
    } else {
      return this.props.children;
    }
  }
}

const mapStateToProps = state => (
  {
    authentication: state.authenticationReducer
  }
)
export default connect(mapStateToProps)(PrivatePage);