import React from 'react';

const Card = (props) => (
  <div
    style={{ display:'flex', flexDirection: 'column' ,alignItems: 'center', justifyContent: 'center',width: '100%', border: '1px solid #ccc', margin: '10px 0', padding: '10px 0', cursor: 'pointer'}}
    onClick={props.onClick}
  >
    {props.children}
  </div>
);

export default Card;
