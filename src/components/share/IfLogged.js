import React from 'react';
import { connect } from 'react-redux';

class IfLogged extends React.Component {
  render() {
    if(this.props.authentication && this.props.authentication.logged) {
      return this.props.children;
    }
    return null;
  }
}

const mapStateToProps = state => (
  {
    authentication: state.authenticationReducer
  }
)
export default connect(mapStateToProps)(IfLogged);