/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require(`path`);

exports.createPages = ({ actions }) => {
  const { createPage } = actions;

  return new Promise((resolve, reject) => {
    createPage({
      path: `/posts/show/*`,
      slug: "id",
      component: path.resolve(`./src/pages/posts/show.js`),
      context: {
        id: ''
      }
    });
    resolve();
  });
};