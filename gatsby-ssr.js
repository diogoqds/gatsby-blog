/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

// You can delete this file if you're not using it
import reduxProvider from './redux-provider';
import { responseInterceptor } from './src/requests/interceptor';

responseInterceptor();
export const wrapRootElement = reduxProvider;