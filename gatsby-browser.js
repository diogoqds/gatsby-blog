/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import reduxProvider from './redux-provider';
import { responseInterceptor } from './src/requests/interceptor';

responseInterceptor();
export const wrapRootElement = reduxProvider;